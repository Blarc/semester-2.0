#include <stdio.h>

int pretvori(int n)
{
    int result = 0;
    int deset = 1;
    while(n > 0) {
        result += (n % 2) * deset;
        n /= 2;
        deset *= 10;
    }
    return result;
}


int main()
{
    unsigned long long int dvojisko = 0;
    int c;
    int rezultat = 0;
    while((c = getchar()) != '\n') {
        if (c <= '9') {
            c -= '0';
        }
        else {
            c -= '7';
        }
        
        dvojisko *= 10000;
        dvojisko += pretvori(c);
        
    }
    
    int dva = 1;
    while(dvojisko > 0) {
        rezultat += (dvojisko % 10) * dva;
        dva *= 2;
        dvojisko /= 10;
    }
    printf("%d\n", rezultat);
}