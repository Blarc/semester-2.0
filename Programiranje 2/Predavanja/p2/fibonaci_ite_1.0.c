//USE ONLY AT YOUR OWN RISK


#include <stdio.h>

int fib(int n)
{
	int prev = 0;
	int curr = 1;

	again:
	if (n == 1) return curr;
	int next = prev + curr;
	prev = curr;
	curr = next;
	n--;
	goto again;
}

int main()
{
	printf("%d\n", fib(44));
}