#include <stdio.h>

int fib(int n); //če importaš mora biti tam tako deklarirano

int main() {
	for(int i = 1; i < 45; i++) printf("%d\n", fib(i));
		return 0;
}

int fib(int n) {
	return((n==1) || (n==2)) ? 1: fib(n-1) + fib(n-2);
}