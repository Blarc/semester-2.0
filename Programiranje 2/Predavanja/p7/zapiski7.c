#include <stdio.h>
#include <stdlib.h>

//REKURZIJA II

//Šahovski konjiček
int moveX[] = {2,1,-1,-2,-2,-1,1,2};
int moveY[] = {1,2,2,1,-1,-2,-2,-1};
//(x,y) -> (x + moveX[m], < y + moveY[m]) m=0..7

int Xsize = 3, Ysize = 4;
int cb[3][4];

//int horse(int n, int x, int y);
//horse(1, 0, 0)

int horse(int n, int x, int y)
{
	if (n == Xsize * Ysize) {
		cb[x][y] = n;
		//izpišemo šahovnico
		for (int i = 0; i < Xsize; i++) {
			for (int j = 0; j < Ysize; j++) {
				printf("%.2d ", cb[i][j]);
			}
			printf("\n");
		}
		cb[x][y] = 0;
		return 1;
	} 
	else {
		printf("Hello!\n");
		int sum = 0;
		cb[x][y] = n;
		for (int m = 0; m < 8; m++) {
			if ((0 <= x + moveX[m]) && (x + moveX[m] < Xsize) &&
				(0 <= y + moveY[m]) && (y + moveY[m] < Ysize) &&
				(cb[x + moveX[m]][y + moveY[m]] == 0)) {
				sum += horse(n + 1, x + moveX[m], y + moveY[m]);
			}
			cb[x][y] = 0;
			return sum;
		}
	}
}

int main()
{
	horse(12,0,0);
	return 0;
}
