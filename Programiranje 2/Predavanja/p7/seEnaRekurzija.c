#include <stdio.h>
#include <stdlib.h>

//HANOISKI STOLPCI
unsigned long stevec = 0;

void hanoi(int n, char src, char tmp, char dst)
{
	stevec++;
	if (n == 1) {
		//printf("%c -> %c\n", src, dst);
	}
	else {
		hanoi(n-1, src, dst, tmp);
		//printf("%c -> %c\n", src, dst); //== hanoi(1, src, tmp, dst)
		hanoi(n-1, tmp, src, dst);
	}
}

int main()
{
	hanoi(16, 'a', 'b', 'c');
	printf("%lu\n", stevec);
	return 0;
}