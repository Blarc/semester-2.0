#include <stdio.h>

int n; n = 10;
printf("%d\n", n);
printf("%d\n", 2 * 5);

scanf("%d", n); 	//NAROBE! n == 10
scanf("%d", &n);	//nastavi lokacijo &n na %d

////////////////////////////////////////////////////

void swap(int a, int b) {
	int t = a;
	a = b;
	b = t;
}

swap(i, j); //i in j ostaneta nespremenjena

////////////////////////////////////////////////////

void swap(int *a, int *b) {
	int t = *a; 
	*a = *b;
	*b = t;
}

swap(&i, &j); //i, j se zamenjata