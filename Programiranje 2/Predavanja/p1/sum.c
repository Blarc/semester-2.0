#include <stdio.h>

int sum (int n)
{
	int s = 0;
	for (int i = 1; i <= n; i++)
		s = s + i;
	return s;
}

int main()
{
	printf("%d\n", sum(10));
	printf("%d\n", sum(100));
	return 0;
}
