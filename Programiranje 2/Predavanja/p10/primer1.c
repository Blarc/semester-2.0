#include <stdio.h>

int main()
{
	FILE* f;
	f = fopen("stevilo.txt", "w");
	if (f == NULL) {
		printf("Nemorem odpreti datoteke\n");
		return 1;
	}

	for (int i = 1; i <= 1000; i++) {
		fprintf(f, "%d\n", i);
	}
	
	if (fclose(f) != 0) {
		printf("Nemorem zapreti datoteke\n");
		return 1;
	}
	return 0;
}