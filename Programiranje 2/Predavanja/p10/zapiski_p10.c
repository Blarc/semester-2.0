VHODNO IZHODNE OPERACIJE

#include <stdio.h>

odpiranje datoteke

FILE* fopen(char* name, char* mode);
	-> "r"; branje
	-> "w"; pisanje
	-> "a"; append

zapiranje datoteke

int fclose(FILE* file);

//PRIMER
#include <stdio.h>

int main()
{
	FILE* f;
	f = fopen("stevilo.txt", "w");
	if (f == NULL) {
		printf("Nemorem odpreti datoteke\n");
		return 1;
	}

	for (int i = 1; i < 1000; i++) {
		fprintf(f, "%d\n", i);
	}

	if (fclose(f) == 0) {
		printf("Nemorem zapreti datoteke\n");
		return 1;
	}
	return 0;
}


//PRIMER 2
#include <stdio.h>
int main(int argc, char* argv[]) {
	if (argc != 2) { }
	FILE *f;
	if ((f = fopen(argv[1], "r")) == NULL) {
		printf("Nemorem odpreti datoteke\n");
		return 1;
	}
////////////////////////////////////////
/* vsako črko posebej */

	int c;
	do {
		c = fgets(f);
		if (c != EOF) {
			printf("%c", c);
		}
	} while (c != EOF);

////////////////////////////////////////
/* po vrsticah */

	char line[1024+1];
	do {
		if (fgets(line, 1024, f) > 0) {
			printf("%s\n", line);
		} else {
			break;
		}
	} while (1);

////////////////////////////////////////
/* ne prebere presledkov */

	char line[1024+1];
	do {
		if (fscanf(f, "%s", line) == 1) {
			printf("%s", line);
		} else {
			break;
		}
	} while (1);

////////////////////////////////////////


	if (fclose(f) != 0) {
		printf("Nemorem zapreti datoteke\n");
		return 1;
	}
	return 0;
}


fread(void* p, int elsize, int numelems, FILE* f);
fwrite(void* p, int elsize, int numelems, FILE* f);

setpos, getpos, ftell, fseek ... MAN ABOUT IT 

#include <stdlib.h>
	exit(int); //OS bo zapru datoteke
	fflush(FILE*);

#include <errno.h>
	int errno;

#include <string.h>
	char* stderor(int);