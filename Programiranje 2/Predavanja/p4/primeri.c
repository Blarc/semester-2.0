//statično pa veliko 40 MB -> DATA

int sum(int n)
{
	static int nums[10000000];		 //lokalno pa (pre)majhno, razen če STATIC
	for(int i = 0; i < n; i++)		 //lokalno -> STACK(sklad)
		nums[i] = i;

	int r = 0;
	for(int i = 0; i < n; i++) 
		r = r + nums[i];

	return r;
}
//vse na sklad

sum(10);		//deluje
sum(100);		//deluje
sum(10000000);  //počasi, vendar deluje
sum(20000000);  //prvih deset milijonov izpise, drugo polovico piše naprej

int sum(int n)
{
	int nums[n];
	for (int i = 0; i < n; i++)
		nums[i] = i;			//lokalna spremenljivka (dela za majhne n-je)
	int r = 0;					//static ne dela, zato se tako NE dela
	for (int i = 0; i < n; i++) //prevajalnik??
		r = r + nums[i];
	return r;
}

//HEAP(kopica) - dinamične spremenljivke - VELIKO

int sum(int n)
{
	int *nums;
	nums = (int*)malloc(n * sizeof(int)); //malloc vrne prostor v pomn. kjer je
										  //n * sizeof(int) bajtov prostora
										  //40 milijonov bajtov, ki so inti(tabela)
	if(nums = NULL) exit(1);			  //če nima prostora, vrne null
	// to je v javi: int[] nums = new int[n];
	//8 bajtov na skladu, 40 milijonov na kopici
	/*
	...
	*/
	free(nums);		//nums na pomnilniku ne rabimo več, zato sprostimo pomnilnik
	return r;		//ves čas bomo delali z istim pomnilnikom
}

//MEMORY LEAK - ko nam zmanjka pomnilnika, aka. "out of memory"

int sum(int n)
{
	static int *nums = NULL;
	static int length = 0;		//v intih
	if (n > length) {
		if (nums != NULL) free(nums);
		length = n;
		nums = (int*)malloc(n * sizeof(int));
		if (nums == NULL) exit(1);
	} else if (n == -1) { free(nums); return -1; }
	-------------> for()...
				   int r = 0;
				   for()...
	return r;
}

////////////////////////////////////////////////////////////////////////////////

int sum(int nums[], int n)
{
	for(int i =...)...
	int r = 0;
	for(int i =...)...
	return r;
}


//enako
int sum(int *nums, int n){...}
int sum(int nums[100], int n){...}
int sum(int nums[0], int n){...}

int tab[1000];
. 
.
.
sum(tab, 50);
sum(NULL, 0); //NAPAKE, na null ne smemo pisat

sum(&(tab[30]), 50);

//////////////////////////////////////////////////////////////////////////////////

int tab[1000];
	tab[10] + tab[20]
int *pi = &(tab[0]);
	pi[10] + pi[20]
==	*(pi + 10)  *(pi + 20)
		   10 * sizeof(int)

lahko tudi: pi++; ali pi--;
++ se nanaša na velikost tipa, v tem primeru 4;