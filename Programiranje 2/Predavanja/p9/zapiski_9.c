22.5 - 2. kolokvij

REKURZIVNE POD.STRUKTURE: SEZNAMI

kazalec l -> podatek|kazalec -> podatek|kazalec ->

struct node
{
	int data;
	struct node* next;
}

typedef struct node
{
	int data;
	struct node* next;
} node; //nov tip z imenom NODE

node* insertA(int a, node* b)
{
	node* n = (node*)malloc(sizeof(node));
	n -> data = d;
	n -> next = l;
	return n;
}

//NAROBE
node* insertZ(int d, node* l)
{
	if (l == NULL) {
		node* n = (node*)malloc(sizeof(node));
		n -> data = d;
		n -> next = NULL;
		l -> next = n;
	} else {
		insertZ(d, l -> next);
	}
}

//PRAVILNO
node* insertZ(int d, node* l)
{
	if (l == NULL) {
		node* n = (node*)malloc(sizeof(node));
		n -> data = d;
		n -> next = NULL;
		return n;
	} else {
		l -> next = insertZ(d, l -> next);
		return l;
	}
}

void deleteAll(node* l)
{
	if (l == NULL) return;
	else {
		deleteAll(l -> next);
		free(l);
	}
}

void safeDeleteAll(node* l)
{
	while (l != NULL) {
		node* next = l -> next;
		free(l);
		l = next;
	}
}

node* insertA(int d, node* l)
{
	node* n = (node*)malloc(sizeof(node));
	n -> date = d;
	n -> left = NULL;
	n -> right = l;
	if (l != NULL) l -> left = n;
	return n;
}

node* insertZ(int d, node* l)
{
	if (l == NULL) return insertA(d, l);
}


VHODNO IZHODNE OPERACIJE

#include <stdio.h>
C:\dir\dir\filename.tip
/dir/dir/filename

FILE*
//tip spremenljivke, ki je vezana na eno fizično datoteko (na disku)

FILE* stdin;	//tipkovnica
FILE* stdout;	//ukazni lupini, okno v katero program izpisuje
FILE* stderr;	//isto okno kot stdout, errors

printf("%d", i);
fprintf(stdout, "%d", i);
fprintf(stderr, "%s\n", i); //gre

fscanf(stdin, "%d", &i);