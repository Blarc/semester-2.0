#include <stdio.h>

int main()
{
	int* comb1[7];			//tabela 7 kazalcev na int
	int (*comb2)[7];		//kazalec na tabelo 7 intov
	int* (comb3[7]);		//tabela 7 kazalcev na int

	printf("%ld\n%ld\n%ld\n", sizeof(comb1), sizeof(comb2), sizeof(comb3));
}