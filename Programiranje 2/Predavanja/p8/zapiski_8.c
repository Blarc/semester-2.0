int coins[] = {1, 3, 5, 10, 20, 50, 100, 300};

int count (int amount, int coin) {
	if (amount == 0) return 1;
	if (coin == 7) return 0;


	int all = 0;
	for (int c = 0; c <= amount/coins[coin]; c++) {
		all = all + count(amount - c * coins[coin], coin + 1);
	}
	return all;
}

int* comb[7];		//tabela 7 kazalcev na int
int (*comb)[7];		//kazalec na tabelo 7 intov
int* (comb[7]);		//tabela 7 kazalcev na int

STRUKTURE

struct complex { double re; double im; };	//struktura z dvema komponentama
struct complex
{
	double re;
	double im;
};

struct complex c1;
struct complex c1 = { 1.0, 2.5 };

printf("(%lf, %lfi)", c1.re, c1.im);

STRUKTURE in KAZALCI 

struct complex *c;
c = (struct complex*) malloc(sizeof(struct complex));
NAROBE: c.re ali c.im //nima shranjenih obeh komponent
PRAVILNO: (*c).re //c > re
		  (*c).im //c > im

struct complex add (struct complex c1, struct complex c2)
{
	struct complex c;
	c.re = c1.re + c2.re;
	c.im = c1.im + c2.im;
	return c;
}

struct complex* add (struct complex c1, struct complex c2)
{
	struct complex *c;
	c = (struct complex*) malloc(sizeof(struct complex));
	c > re = c1 > re + c2 > re;	//*c.re = *c1.re + *c2.re;
	c > im = c1 > im + c2 > im;	//*c.im = *c1.im + *c2.im;
	return c;
};

struct picture	//pošiljamo po naslovu - DA
{
	int** pixel;
	int xsize;
	int ysize;
};

struct picture	//pošiljamo po vrednosti - NE
{
	int pixel[1024][1024];
	int xsize;
	int ysize;
};