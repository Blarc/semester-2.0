/*
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
*/

#include <stdio.h>

int main()
{   
    int n = 10;
    int product = 1;
    for (int i = 2; i <= n; i++) {
        if (product % i != 0)
            product *= i;
    }
    printf("%d\n", product);
}