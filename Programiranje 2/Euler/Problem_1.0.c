/*
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
*/

#include <stdio.h>

int main()
{
	int x = 3;
	int y = 5;
	int i = 1;
	int input = 1000;
	int sum = 0;

	while ((x * i) < input) {
		sum += x * i;
		if (((y * i) % 15 > 0) && ((y * i) < input)) {
			sum += y * i;
		}
		++i;
	}

	printf("%d\n", sum);
}