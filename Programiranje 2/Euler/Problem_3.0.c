/*
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
*/

#include <stdio.h>

int fun(long n)
{
	for (int i = 2; i < n; i++) {
		if (n % i == 0)
			return i;
	}
	return n;
}

int main()
{	
	long n = 600851475143;
	while (fun(n) != n) {
		n = n / fun(n);
		printf("%ld\n", n);
	}
}

