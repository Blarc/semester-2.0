#include <stdio.h>
#include <stdlib.h>

typedef struct predmet
{
	int prostor;
	int cena;

} predmet;

int fun(int skp_prostor, predmet* predmeti, int i, int d)
{
	int cena1 = 0, cena2 = 0;
	//printf("%d\n", skp_prostor);
	if (skp_prostor <= 0 || i == d) {
		return 0;
	}

	if (skp_prostor - predmeti[i].prostor >= 0) {
		cena1 = fun(skp_prostor - predmeti[i].prostor, predmeti, i+1, d) + predmeti[i].cena;
	}

	cena2 = fun(skp_prostor, predmeti, i+1, d);

	if (cena1 < cena2) {
		return cena2;
	} else {
		return cena1;
	}

}

int main()
{
	int skp_prostor, n;
	scanf("%d", &skp_prostor);
	scanf("%d", &n);

	predmet* predmeti = malloc(n * sizeof(predmet));
	for (int i = 0; i < n; i++) {
		scanf("%d", &predmeti[i].prostor);
	}
	for (int i = 0; i < n; i++) {
		scanf("%d", &predmeti[i].cena);
	}

	int result = fun(skp_prostor, predmeti, 0, n);
	printf("%d\n", result);

	free(predmeti);
	return 0;
}