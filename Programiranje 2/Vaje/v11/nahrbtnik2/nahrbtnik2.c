#include <stdio.h>
#include <stdlib.h>

int MEMO[1001][1001][2];

typedef struct predmet
{
	int prostor;
	int cena;

} predmet;

int fun(int skp_prostor, predmet* predmeti, int i, int d, int liho)
{
	int najCena = 0, cena2 = 0;
	//printf("%d\n", skp_prostor);
	if (/*skp_prostor <= 0 ||*/ i == d) {
		return 0;
	}

	if (MEMO[i][skp_prostor][liho] > 0) {
		return MEMO[i][skp_prostor][liho];
	}

	if (skp_prostor - predmeti[i].prostor >= 0) {
		if (predmeti[i].prostor % 2 == 1 && liho != 1) {
			najCena = fun(skp_prostor - predmeti[i].prostor, predmeti, i+1, d, 1) + predmeti[i].cena;
		} else if (predmeti[i].prostor % 2 != 1) {
			najCena = fun(skp_prostor - predmeti[i].prostor, predmeti, i+1, d, liho);
		}
	}

	cena2 = fun(skp_prostor, predmeti, i+1, d, liho);

	if (najCena < cena2) {
		najCena = cena2;
	}

	MEMO[i][skp_prostor][liho] = najCena;

	return najCena;

}

//memoizacijo - argumenti, ki se spreminjajo (v tem primeru skp_prostor ter i)

int main()
{
	int skp_prostor, n;
	scanf("%d", &skp_prostor);
	scanf("%d", &n);

	predmet* predmeti = malloc(n * sizeof(predmet));
	for (int i = 0; i < n; i++) {
		scanf("%d", &predmeti[i].prostor);
	}
	for (int i = 0; i < n; i++) {
		scanf("%d", &predmeti[i].cena);
	}

	int result = fun(skp_prostor, predmeti, 0, n, 0);
	printf("%d\n", result);

	free(predmeti);
	return 0;
}