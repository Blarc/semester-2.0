#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAKS_ST_STUDENTOV 1000
#define MAKS_ST_PREDMETOV 20

typedef struct PO {
    char predmet[4];
    int ocena;
} PO;
typedef struct Student {
    int vpisna;
    PO* po;
    int stPO;
} Student;
int poisciStudenta(Student ** studenti, int stStudentov, int vpisna){
    for(int i=0; i<stStudentov; i++){
        if(studenti[i]->vpisna == vpisna) return i;
    }
    return -1;
}
int poisciPO(Student* student, char* predmet) {
    for(int i=0; i<student->stPO; i++){
        if(strcmp(predmet, student->po[i].predmet) == 0) return i;
    }
    return -1;
}
int main(){
    Student** studenti = malloc(MAKS_ST_STUDENTOV*sizeof(Student*));
    int stStudentov = 0;
    int vpisna, ocena;
    char predmet[4];
    while(scanf("%d%s%d", &vpisna, predmet, &ocena)!=EOF){
        //POIŠČI ŠTUDENTA
        int ixStudenta = poisciStudenta(studenti, stStudentov, vpisna);
        //ČE GA NI GA DODAJ
        if(ixStudenta < 0){
            Student* novi = malloc(sizeof(Student));
            novi->vpisna = vpisna;
            novi->po = malloc(MAKS_ST_PREDMETOV * sizeof(PO));
            novi->stPO = 0;
            studenti[stStudentov] = novi;
            ixStudenta = stStudentov;
            stStudentov++;
        }
        //POIŠČI PREDMET_OCENA ZA ŠTUDENTA
        Student* student = studenti[ixStudenta];
        int ixPO = poisciPO(student, predmet);
        //ČE TEGA NI GA DODAJ
        if(ixPO < 0){
            ixPO = student->stPO;
            strcpy(student->po[ixPO].predmet, predmet);
            student->stPO++;
        }
        //ČE JE PREDMET NOTER POSODOBI OCENO
        student->po[ixPO].ocena = ocena;
    }
    //IZPIS
    for(int i=0; i < stStudentov; i++){
        Student* student = studenti[i];
        printf("%d:", student->vpisna);
        for(int j = 0; j < student->stPO; j++){
            PO po = student ->po[j];
            printf(" %s/%d", po.predmet, po.ocena);
        }
        printf("\n");
        free(student->po);
        free(student);        
    }
    free(studenti);
    return 0;
}
