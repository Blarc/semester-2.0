#include <stdio.h>
#include <stdlib.h>

int main()
{
	char* ime_vhod = malloc(21 * sizeof(char));
	char* ime_izhod = malloc(21 * sizeof(char));
	FILE* vhod;
	FILE* izhod;

	int i = 0;
	char c;
	while((c = getchar()) != '\n') {
		ime_vhod[i] = c;
		i++;
	}
	ime_vhod[i] = '\0';

	i = 0;
	while((c = getchar()) != '\n') {
		ime_izhod[i] = c;
		i++;
	}
	ime_izhod[i] = '\0';

	/*printf("%s\n", ime_vhod);
	printf("%s\n", ime_izhod);*/

	vhod = fopen(ime_vhod, "r");
	izhod = fopen(ime_izhod, "w");

	int sirina, visina;
	fscanf(vhod, "P6\n%d%d\n255\n", &sirina, &visina);
	//printf("%d\n%d\n", sirina, visina);

	unsigned char* tabela = malloc(sirina * visina * 3 * sizeof(unsigned char));
	fread(tabela, sizeof(unsigned char), sirina*visina*3, vhod);

	int vrednost = 0;
	for (i = 0; i < sirina*visina*3; i += 3) {
		for (int j = 0; j < 3; j++) {
			vrednost += tabela[i+j];
		}
		vrednost /= 3;
		if (230 <= vrednost && vrednost <= 255) {
			fprintf(izhod, "%c", ' ');
		}
		else if (200 <= vrednost && vrednost <= 299) {
			fprintf(izhod, "%c", '.');
		}
		else if (180 <= vrednost && vrednost <= 199) {
			fprintf(izhod, "%c", '\'');
		}
		else if (160 <= vrednost && vrednost <= 179) {
			fprintf(izhod, "%c", ':');
		}
		else if (130 <= vrednost && vrednost <= 159) {
			fprintf(izhod, "%c", 'o');
		}
		else if (100 <= vrednost && vrednost <= 129) {
			fprintf(izhod, "%c", '&');
		}
		else if (70 <= vrednost && vrednost <= 99) {
			fprintf(izhod, "%c", '8');
		}
		else if (50 <= vrednost && vrednost <= 69) {
			fprintf(izhod, "%c", '#');
		}
		else {
			fprintf(izhod, "%c", '@');
		}
		if ((i/3) % sirina == 0 && i != 0) {
			fprintf(izhod, "%c ", '\n');
		}
		vrednost = 0;
	}
	fprintf(izhod, "%c", '\n');
}