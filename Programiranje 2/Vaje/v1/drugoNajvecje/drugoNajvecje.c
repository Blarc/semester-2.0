#include <stdio.h>

int main()
{
	int max = -1;
	int smax = -1;
	int n = getchar() -'0';
	getchar();

	for (int i = 0; i < n; i++) {
		int x = getchar() -'0';
		if (x > max) {
			smax = max;
			max = x;
		}

		else if (x > smax) {
			smax = x;
		}
		getchar();
	}

	putchar(smax + '0');
	putchar('\n');

	//gcc -std=gnu99 or c99
}