#include <stdio.h>
#include <stdlib.h>


typedef struct oseba {
	int stKnjig;
	char* ime;
} oseba;


int main()
{
	int n, k;
	scanf("%d %d", &n, &k);

	oseba* osebe = malloc(n * sizeof(oseba));
	for (int i = 0; i < n; i++) {
		osebe[i].ime = malloc(17 * sizeof(char));
		scanf("%s", osebe[i].ime);
	}
	for (int i = 0; i < n; i++) {
		scanf("%d", &osebe[i].stKnjig);
	}

	int i = 1;
	while (i < n) {
		int j = i;
		while (j > 0 && osebe[j-1].stKnjig < osebe[j].stKnjig) {
			oseba x = osebe[j];
			osebe[j] = osebe[j-1];
			osebe[j-1] = x;
			j--;
		}
		i++;
	}

	int stevec = 1, stevec2 = 0;
	for (int i = 0; stevec <= k && stevec <= n; i++) {
		printf("%d. %s (%d)\n", stevec, osebe[i].ime, osebe[i].stKnjig);
		if (osebe[i].stKnjig != osebe[i+1].stKnjig) {
			stevec++;
			stevec += stevec2;
			stevec2 = 0;
		} else {
			stevec2++;
		}
	}

}