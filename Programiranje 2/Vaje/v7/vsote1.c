#include <stdio.h>
#include <stdlib.h>

long MEMO[401][401];

long nacini(int vsota, int najsumand)
{
	if (vsota == 0) {
		return 1;
	}
	else if (vsota < 0 || najsumand == 0) {
		return 0;
	}
	if (MEMO[vsota][najsumand] != 0) {
		return MEMO[vsota][najsumand];
	}
	long z = nacini(vsota - najsumand, najsumand);
	long brez = nacini(vsota, najsumand - 1);
	long stNacinov = z + brez;
	MEMO[vsota][najsumand] = stNacinov;
	return stNacinov;
}

int main()
{	
	int a, b;
	scanf("%d%d", &a, &b);
	printf("%ld\n", nacini(a, b));
	return 0;
}
