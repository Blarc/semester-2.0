#include <stdio.h>
#include <stdlib.h>

void izpisi(int* t, int stevec) {
	for (int i = 0; i < stevec; i++) {
		if (i > 0) {
			printf(" + ");
		}
		printf("%d", t[i]);
	}
	printf("\n");
}

long nacini(int vsota, int najsumand, int* tab, int stevec)
{

	//printf("nacini(%d %d)\n", vsota, najsumand);
	if (vsota == 0) {
		izpisi(tab, stevec);
		return 1;
	}
	else if (vsota < 0 || najsumand <= 0) {
		return 0;
	}

	//primeri ko vkljucimo najvecji sumand
	tab[stevec] = najsumand;
	long z = nacini(vsota - najsumand, najsumand, tab, stevec+1);
	
	//primeri ko ne vkljucimo nobenega sumanda, samo odstranimo najvecjega
	long brez = nacini(vsota, najsumand - 1, tab, stevec);
	long stNacinov = z + brez;

	return stNacinov;
}

int main()
{	
	int vsota, najsumand;
	scanf("%d%d", &vsota, &najsumand);
	int* tab = (int*)malloc(a * sizeof(int));
	printf("%ld\n", nacini(a, b, tab, 0));
	return 0;
}
