#include <stdio.h>
#include <stdlib.h>

#include "naloga.h"

void indeksInKazalec(int* t, int* indeks, int** kazalec) {
    
}

void frekvenceCrk(char* niz, int** frekvence) {
    int* pogostosti = calloc(26, sizeof(int));
    char* p = niz;
    while(*p != '\0'){
    	char znak = *p;
    	if(znak >= 'A' && znak <= 'z'){
    	
	    	if(znak < 'a'){
	    		pogostosti[(int)znak - 'A']++;
	    	} else {
	    		pogostosti[(int)znak - 'a']++;
	    	}
	}
    	p++;
    }
    *frekvence = pogostosti;
}

int** preoblikuj(int** t, int dolzinaVrstice) {
    //prestej elemente
    int i = 0;
    int stElementov = 0;
    while(t[i] != NULL){
    	int j = 0;
    	while(t[i][j] > 0){
    		stElementov++;
    		j++;
    	}
    	i++;
    }
    //alociraj tabele - pripravi strukturo
    int stVrstic = (stElementov / dolzinaVrstice) + (stElementov % dolzinaVrstice != 0);
    int** rezultat = malloc((stVrstic+1) * sizeof(int*));
    for(int i=0; i<stVrstic; i++){
    	int dv = dolzinaVrstice;
    	if(i==stVrstic-1 && (stElementov % dolzinaVrstice != 0)){
    		dv = stElementov % dolzinaVrstice;
    	}
    	rezultat[i] = malloc((dv+1) *sizeof(int));
    	rezultat[i][dv] = 0;
    }
    
    //vpisi vrednosti v novo strukturo
    i = 0;
    stElementov = 0;
    while(t[i] != NULL){
    	int j = 0;
    	while(t[i][j] > 0) {
    	        rezultat[stElementov/dolzinaVrstice][stElementov % dolzinaVrstice] = t[i][j];
    	        stElementov++;
    		j++;
    	}
    	i++;
    }
     
    return rezultat;
}

int main() {
    // koda za ro"cno testiranje (po "zelji)

    return 0;
}

https://pastebin.com/FQViXgqb