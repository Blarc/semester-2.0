
#include <stdio.h>
#include <stdlib.h>

#include "naloga.h"

int* TABELA[] = {
    (int[]) {10, 20, 30, 40, 50, 0},
    (int[]) {60, 70, 80, 0},
    (int[]) {90, 100, 110, 120, 130, 0},
    NULL,
};

int __main__() {

    int** r = preoblikuj(TABELA, 5);
    int i = 0;
    while (r[i] != NULL) {
        int j = 0;
        printf("[");
        while (r[i][j] > 0) {
            if (j > 0) {
                printf(", ");
            }
            printf("%d", r[i][j]);
            j++;
        }
        printf("]\n");
        free(r[i]);
        i++;
    }
    free(r);

    exit(0);
    return 0;
}
