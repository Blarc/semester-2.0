
#include <stdio.h>
#include <stdlib.h>

#include "funkcije.h"

char NIZ[1000];
int TABELA1[] = {42, 17, 20, 15, 6, 0, 9, 13, 35, 25};
int TABELA2[] = {2, 4, 6, 8, 10, 12};
int TABELA3[] = {5, 1, 3, 7};
int TABELA4[] = {};

char* izpisi(int* t, int n, char* cilj) {
    char* p = cilj;
    p += sprintf(p, "[");
    for (int i = 0;  i < n;  i++) {
        if (i > 0) {
            p += sprintf(p, ", ");
        }
        p += sprintf(p, "%d", t[i]);
    }
    p += sprintf(p, "]");
    return cilj;
}

bool sodo(int stevilo) {
    return (stevilo % 2 == 0);
}

void pozeni(int* tabela, int dolzina) {
    printf("%s -> %d\n", 
            izpisi(tabela, dolzina, NIZ),
            prestej(tabela, dolzina, sodo));
}

int __main__() {
    pozeni(TABELA1, sizeof(TABELA1) / sizeof(int));
    pozeni(TABELA2, sizeof(TABELA2) / sizeof(int));
    pozeni(TABELA3, sizeof(TABELA3) / sizeof(int));
    pozeni(TABELA4, sizeof(TABELA4) / sizeof(int));

    exit(0);
    return 0;
}
