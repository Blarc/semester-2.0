
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "funkcije.h"

char NIZ[1000];

char* TABELA1[] = {"O", "Vrba", "srecna", "draga", "vas", "domaca"};
char* TABELA2[] = {"studiram", "na", "fakulteti", "za", "racunalnistvo", "in", "informatiko"};
char* TABELA3[] = {"", "aaaa", "aaaaaaaa", "aaaaaaaaaaaaaaaa", "aaaaaaaaaaaa", "aa", "aaaaaa", "aaaaaaaaaa"};
char* TABELA4[] = {"en_sam_niz"};

char* izpisi(char** t, int n, char* cilj) {
    char* p = cilj;
    p += sprintf(p, "[");
    for (int i = 0;  i < n;  i++) {
        if (i > 0) {
            p += sprintf(p, ", ");
        }
        p += sprintf(p, "%s", t[i]);
    }
    p += sprintf(p, "]");
    return cilj;
}

bool jeDaljsi(void* a, void* b) {
    return (strlen(*(char**) a) > strlen(*(char**) b));
}

void pozeni(char** tabela, int dolzina) {
    printf("%s -> %d\n",
            izpisi(tabela, dolzina, NIZ),
            indeksNajmanjsega(tabela, sizeof(char*), dolzina, jeDaljsi));
}

int __main__() {
    pozeni(TABELA1, sizeof(TABELA1) / sizeof(char*));
    pozeni(TABELA2, sizeof(TABELA2) / sizeof(char*));
    pozeni(TABELA3, sizeof(TABELA3) / sizeof(char*));
    pozeni(TABELA4, sizeof(TABELA4) / sizeof(char*));

    exit(0);
    return 0;
}
