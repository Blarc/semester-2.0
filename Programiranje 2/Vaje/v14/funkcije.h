
#include <stdbool.h>

typedef bool(*I2B)(int);
typedef int(*I2I)(int);
typedef int(*II2I)(int, int);
typedef bool(*VV2B)(void*, void*);

int prestej(int* tabela, int dolzina, I2B predikat);
int* pretvori(int* tabela, int dolzina, I2I pretvornik);
int zdruzi(int* tabela, int dolzina, II2I zdruzevalec, int zacetnaVrednost);
int indeksNajmanjsega(void* tabela, int velikostElementa, int dolzina, VV2B jeManjsi);
