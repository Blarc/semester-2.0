
#include <stdio.h>
#include <stdlib.h>

#include "funkcije.h"

char NIZ[1000];

int TABELA1[] = {5, 7, 2, -4, 8, -3, 1, 4};
int TABELA2[] = {10, 30, 50};
int TABELA3[] = {42};
int TABELA4[] = {};

char* izpisi(int* t, int n, char* cilj) {
    char* p = cilj;
    p += sprintf(p, "[");
    for (int i = 0;  i < n;  i++) {
        if (i > 0) {
            p += sprintf(p, ", ");
        }
        p += sprintf(p, "%d", t[i]);
    }
    p += sprintf(p, "]");
    return cilj;
}

int vsota(int a, int b) {
    return (a + b);
}

void pozeni(int* tabela, int dolzina) {
    printf("%s -> %d\n",
            izpisi(tabela, dolzina, NIZ),
            zdruzi(tabela, dolzina, vsota, 0));
}

int __main__() {
    pozeni(TABELA1, sizeof(TABELA1) / sizeof(int));
    pozeni(TABELA2, sizeof(TABELA2) / sizeof(int));
    pozeni(TABELA3, sizeof(TABELA3) / sizeof(int));
    pozeni(TABELA4, sizeof(TABELA4) / sizeof(int));

    exit(0);
    return 0;
}
