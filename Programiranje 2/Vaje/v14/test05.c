
#include <stdio.h>
#include <stdlib.h>

#include "funkcije.h"

char NIZ1[1000];
char NIZ2[1000];

int TABELA1[] = {17, 384, 522673, 0, 123456789, 4519123, 28241961, 1024, 531441, 6};
int TABELA2[] = {8585, 46135, 794, 3};
int TABELA3[] = {42};
int TABELA4[] = {};

char* izpisi(int* t, int n, char* cilj) {
    char* p = cilj;
    p += sprintf(p, "[");
    for (int i = 0;  i < n;  i++) {
        if (i > 0) {
            p += sprintf(p, ", ");
        }
        p += sprintf(p, "%d", t[i]);
    }
    p += sprintf(p, "]");
    return cilj;
}

int steviloStevk(int stevilo) {
    int n = 0;
    while (stevilo > 0) {
        stevilo /= 10;
        n++;
    }
    return n;
}

void pozeni(int* tabela, int dolzina) {
    int* rezultat = pretvori(tabela, dolzina, steviloStevk);

    printf("%s -> %s\n", 
            izpisi(tabela, dolzina, NIZ1),
            izpisi(rezultat, dolzina, NIZ2));
    free(rezultat);
}

int __main__() {
    pozeni(TABELA1, sizeof(TABELA1) / sizeof(int));
    pozeni(TABELA2, sizeof(TABELA2) / sizeof(int));
    pozeni(TABELA3, sizeof(TABELA3) / sizeof(int));
    pozeni(TABELA4, sizeof(TABELA4) / sizeof(int));

    exit(0);
    return 0;
}
