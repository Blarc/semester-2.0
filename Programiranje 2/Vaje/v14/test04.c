
#include <stdio.h>
#include <stdlib.h>

#include "funkcije.h"

char NIZ1[1000];
char NIZ2[1000];

int TABELA1[] = {17, -6, 12, 10, 8, 0, 1, -5, 9, 20};
int TABELA2[] = {-10, -20, -30, -5};
int TABELA3[] = {42};
int TABELA4[] = {};

char* izpisi(int* t, int n, char* cilj) {
    char* p = cilj;
    p += sprintf(p, "[");
    for (int i = 0;  i < n;  i++) {
        if (i > 0) {
            p += sprintf(p, ", ");
        }
        p += sprintf(p, "%d", t[i]);
    }
    p += sprintf(p, "]");
    return cilj;
}

int kvadriraj(int stevilo) {
    return (stevilo * stevilo);
}

void pozeni(int* tabela, int dolzina) {
    int* rezultat = pretvori(tabela, dolzina, kvadriraj);

    printf("%s -> %s\n", 
            izpisi(tabela, dolzina, NIZ1),
            izpisi(rezultat, dolzina, NIZ2));
    free(rezultat);
}

int __main__() {
    pozeni(TABELA1, sizeof(TABELA1) / sizeof(int));
    pozeni(TABELA2, sizeof(TABELA2) / sizeof(int));
    pozeni(TABELA3, sizeof(TABELA3) / sizeof(int));
    pozeni(TABELA4, sizeof(TABELA4) / sizeof(int));

    exit(0);
    return 0;
}
