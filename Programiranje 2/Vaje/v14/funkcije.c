
#include <stdio.h>
#include <stdlib.h>

#include "funkcije.h"

int prestej(int* tabela, int dolzina, I2B predikat) {
	int stevec = 0;
	for (int i = 0; i < dolzina; i++) {
		if (predikat(tabela[i])) {
			stevec++;
		}
	}
	return stevec;
}

int* pretvori(int* tabela, int dolzina, I2I pretvornik) {
    int* novaTabela = malloc(dolzina * sizeof(int));
    for (int i = 0; i < dolzina; i++) {
    	novaTabela[i] = pretvornik(tabela[i]);
    }
    return novaTabela;
}

int zdruzi(int* tabela, int dolzina, II2I zdruzevalec, int zacetnaVrednost) {
	int skupek = zacetnaVrednost;
	for (int i = dolzina-1; i >= 0; i--) {
		skupek = zdruzevalec(tabela[i], skupek);
	}
	return skupek;
}

int indeksNajmanjsega(void* tabela, int velikostElementa, int dolzina, VV2B jeManjsi) {
	int boolean;
	for (int i = 0; i < dolzina*velikostElementa; i+=velikostElementa) {
		for (int j = 0; j < dolzina*velikostElementa; j+=velikostElementa) {
			if (j != i) {
				if (!jeManjsi((tabela+j), (tabela+i))) {
					boolean = 1;
				}
				else {
					boolean = 0;
					break;
				}
			}
		}
		if (boolean) {
			return (i / velikostElementa);
		}
	}
	return 0;
}

int main() {
    return 0;
}
