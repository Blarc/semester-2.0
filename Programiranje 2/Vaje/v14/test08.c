
#include <stdio.h>
#include <stdlib.h>

#include "funkcije.h"

char NIZ[1000];

int TABELA1[] = {42, 17, -20, -15, -6, 0, -9, -13, -35, 25};
int TABELA2[] = {-10, -20, -30, -5};
int TABELA3[] = {10, 20, 30, 5};
int TABELA4[] = {-15};

char* izpisi(int* t, int n, char* cilj) {
    char* p = cilj;
    p += sprintf(p, "[");
    for (int i = 0;  i < n;  i++) {
        if (i > 0) {
            p += sprintf(p, ", ");
        }
        p += sprintf(p, "%d", t[i]);
    }
    p += sprintf(p, "]");
    return cilj;
}

bool jeManjsi(void* a, void* b) {
    return (*(int*) a) < (*(int*) b);
}

void pozeni(int* tabela, int dolzina) {
    printf("%s -> %d\n",
            izpisi(tabela, dolzina, NIZ),
            indeksNajmanjsega(tabela, sizeof(int), dolzina, jeManjsi));
}

int __main__() {
    pozeni(TABELA1, sizeof(TABELA1) / sizeof(int));
    pozeni(TABELA2, sizeof(TABELA2) / sizeof(int));
    pozeni(TABELA3, sizeof(TABELA3) / sizeof(int));
    pozeni(TABELA4, sizeof(TABELA4) / sizeof(int));

    exit(0);
    return 0;
}
