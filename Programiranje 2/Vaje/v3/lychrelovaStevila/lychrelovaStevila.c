#include <stdio.h>

long long zrcali(long long n)
{
    long long z = 0;
    while (n > 0) {
        z = z * 10 + n % 10;
        n /= 10;
    }
    return z;
}

int lychrel(long long n, int k)
{
    for (int i = 0; i < k && n < 1e17; i++) {
        n += zrcali(n);
        if (n == zrcali(n)) return 0;
    }
    return 1;
}

int main()
{
    int k, a, b, st=0;
    scanf("%d %d %d", &k, &a, &b);
    for (int i = a; i <= b; i++) {
        st += lychrel(i, k);
    }
    printf("%d\n", st);
    return 0;
}