#include <stdio.h>

int delit(int x)
{   
    int n = 1;
    int vsota = 0;
    while (n < x) {
        if (x % n == 0) {
            vsota += n;
        }
        n++;
    }
    return vsota;
}


int main()
{   
    int y;
    scanf("%d", &y);
    if (y != delit(delit(y)) || y == delit(y)) {
        printf("NIMA");
    }
    else {
        printf("%d\n", delit(y));
    }
}