#include <stdio.h>
#include <stdlib.h>

/*
int a;
FILE* f = fopen("ime_datoteke.txt", "r");
fscanf(f, "%d", &a);

FILE* f1 = fopen("ime_datoteke.txt", "w");
fprintf(f1, "%d", a);

FILE** kazalci = malloc(5 * sizeof(FILE*));
*kazalci = fopen("datoteka.txt", "r");

fclose(f);
*/

int main()
{
	int n, stevec = 0;
	scanf("%d", &n);
	FILE** datoteke = malloc((n+1) * sizeof(FILE*));
	char* ime_datoteke = malloc(101 * sizeof(char));

	for (int i = 0; i < n; i++) {
		scanf("%s", ime_datoteke);
		datoteke[i] = fopen(ime_datoteke, "r");
	}

	scanf("%s", ime_datoteke);
	datoteke[n] = fopen(ime_datoteke, "w");

	int* stevilke = malloc(n * sizeof(int));
	for (int i = 0; i < n; i++) {
		fscanf(datoteke[i], "%d\n", &stevilke[i]);
	}

	while (stevec != n)
	{
		int min = 1000000001;
		int indeks = -1;
		for (int i = 0; i < n; i++) {
			if (stevilke[i] < min && stevilke[i] != -1) {
				min = stevilke[i];
				indeks = i;
			}
		}

		if (stevilke[indeks] > 0) {
			fprintf(datoteke[n], "%d\n", stevilke[indeks]);
		}

		if (fscanf(datoteke[indeks], "%d\n", &stevilke[indeks]) < 1) {
			stevilke[indeks] = -1;
			stevec++;
		}
		min = 1000;
		indeks = -1;
	}

	for (int i = 0; i <= n; i++) {
		fclose(datoteke[i]);
	}

	free(datoteke);
	free(ime_datoteke);
	free(stevilke);

	return 0;
}