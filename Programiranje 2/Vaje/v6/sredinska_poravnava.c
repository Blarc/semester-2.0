#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void dodaj(char* beseda, char* vrstica, int stevec)
{
	for(int i = 0; i < strlen(beseda); i++) {
		vrstica[stevec] = beseda[i];
	}
	vrstica[stevec + strlen(beseda)] = '\0';
}

void izpisi(int n, char* vrstica)
{
	int stPresledkov = (n - strlen(vrstica)) / 2;
	for (int i = 0; i < n; i++) {
		printf(" ");
	}

	printf("%s", vrstica);
	printf("\n");


}

int main()
{
	int n, stevec = 0, m = 0;
	scanf("%d", &n);

	char* beseda = (char*)malloc((n+1) * sizeof(char));
	char* vrstica =(char*)malloc((n+1) * sizeof(char));

	while(scanf("%s", beseda) == 1) {
		if ((stevec + strlen(beseda) + (stevec > 0)) < n) {
			dodaj(beseda, vrstica, stevec);
			stevec += strlen(beseda) + (stevec > 0);
		}
		else {
			izpisi(n, vrstica);
			dodaj(beseda, vrstica, 0);
			stevec = strlen(beseda);
		}
	}
	izpisi(n, vrstica);

	free(beseda);
	free(vrstica);
	return 0;
}