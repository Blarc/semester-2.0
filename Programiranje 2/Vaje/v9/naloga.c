
#include "naloga.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

char NIZ[1000];

char* vNiz(Vozlisce* zacetek) {
    char* pNiz = NIZ;
    *pNiz++ = '[';
    Vozlisce* v = zacetek;
    while (v != NULL) {
        if (v != zacetek) {
            pNiz += sprintf(pNiz, ", ");
        }
        pNiz += sprintf(pNiz, "%d", v->podatek);
        v = v->naslednje;
    }
    sprintf(pNiz, "]");
    return NIZ;
}


int vsotaI(Vozlisce* zacetek) {
	int vsota = 0;
    while (zacetek != NULL) {
    	vsota += zacetek -> podatek;
    	zacetek = zacetek -> naslednje;
    }
    return vsota;
}

int vsotaR(Vozlisce* zacetek) {
	if (zacetek == NULL) {
		return 0;
	}
    return vsotaR(zacetek -> naslednje) + zacetek -> podatek;
}

Vozlisce* vstaviUrejenoI(Vozlisce* zacetek, int element) {
	Vozlisce* novo = malloc(sizeof(Vozlisce));
	novo -> podatek = element;
	if (zacetek == NULL) {
		novo -> naslednje = NULL;
		return novo;
	}
	else if (zacetek -> podatek >= element) {
		novo -> naslednje = zacetek;
		return novo;
	}
	else {
		Vozlisce* atm = zacetek;
		while (atm -> naslednje != NULL) {
			if (atm -> naslednje -> podatek > element) {
				novo -> naslednje = atm -> naslednje;
				atm -> naslednje = novo;
				return zacetek;
			}
			atm = atm -> naslednje;
		}
		return zacetek;
	}
}

Vozlisce* vstaviUrejenoR(Vozlisce* zacetek, int element) {
    return NULL;
}

Vozlisce* pobrisiI(Vozlisce* zacetek, int element) {
	/*if (zacetek == NULL) {
		return NULL;
	}
	while (zacetek -> podatek == element) {
		if (zacetek -> naslednje == NULL) {
			free(zacetek);
			return NULL;
		}
		zacetek = zacetek -> naslednje;
		free(zacetek);
	}

	Vozlisce* atm  = zacetek;
	while (atm != NULL) {
		if (atm -> podatek == element) {

		}
	}*/
	return NULL;
}

Vozlisce* pobrisiR(Vozlisce* zacetek, int element) {
	if (zacetek == NULL) {
		return NULL;
	}

	Vozlisce* atm = zacetek;
	printf("Test");
	//vNiz(atm);
	if (atm -> naslednje == NULL) {
		return zacetek;
	}
	if (atm -> naslednje -> podatek == element) {
		Vozlisce* wat = atm -> naslednje -> naslednje;
		free(atm -> naslednje);
		pobrisiR(wat, element);
	}
	return zacetek;
}


Vozlisce* zgradi(int* t) {
    Vozlisce* zacetek = NULL;
    Vozlisce* prejsnje = NULL;
    int* p = t;

    while (*p > 0) {
        Vozlisce* novo = malloc(sizeof(Vozlisce));
        novo->podatek = *p;
        novo->naslednje = NULL;
        if (p == t) {
            zacetek = novo;
        } else {
            prejsnje->naslednje = novo;
        }
        prejsnje = novo;
        p++;
    }
    return zacetek;
}

void pocisti(Vozlisce* v) {
    if (v != NULL) {
        pocisti(v->naslednje);
        free(v);
    }
}

void testiraj(Vozlisce* v, int element) {
    printf("%s", vNiz(v));
    v = pobrisiR(v, element);
    printf(" - %d -> %s\n", element, vNiz(v));
    pocisti(v);
}


int main() {
    testiraj(zgradi((int[]) {5, 5, 5, 7, 5, 7, 7, 7, 0}), 5);
    testiraj(zgradi((int[]) {5, 5, 5, 7, 5, 7, 7, 7, 0}), 7);
    testiraj(zgradi((int[]) {5, 5, 5, 7, 5, 7, 7, 7, 0}), 3);
    testiraj(zgradi((int[]) {42, 42, 42, 42, 42, 42, 0}), 42);
    testiraj(zgradi((int[]) {0}), 42);
    testiraj(zgradi((int[]) {1, 2, 3, 2, 1, 2, 3, 2, 0}), 1);
    testiraj(zgradi((int[]) {1, 2, 3, 2, 1, 2, 3, 2, 0}), 2);
    testiraj(zgradi((int[]) {1, 2, 3, 2, 1, 2, 3, 2, 0}), 3);
    testiraj(zgradi((int[]) {1, 1, 2, 3, 1, 1, 1, 4, 5, 0}), 1);
    testiraj(zgradi((int[]) {2, 3, 1, 1, 1, 4, 5, 1, 1, 0}), 1);

    exit(0);
    return 0;
}


//RESITEV

Vozlisce* pobrisiR(Vozlisce* zacetek, int element) {
    //ustavi če smo na koncu
    if(zacetek = NULL){
      return NULL
    }
    if(zacetek->podatek == element){
      Vozlisce* nasl = zacetek->naslednje;
      free(zacetek);
      return pobrisirR(nasl, element);
    }
    
    //briši če je potrebno
    zacetek->naslednje = pobrisiR(zacetek->naslednje, element);
    return zacetek;
}