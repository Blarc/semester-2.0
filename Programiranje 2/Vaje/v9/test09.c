
#include "naloga.h"

#include <stdio.h>
#include <stdlib.h>

char NIZ[1000];

Vozlisce* zgradi(int* t) {
    Vozlisce* zacetek = NULL;
    Vozlisce* prejsnje = NULL;
    int* p = t;

    while (*p > 0) {
        Vozlisce* novo = malloc(sizeof(Vozlisce));
        novo->podatek = *p;
        novo->naslednje = NULL;
        if (p == t) {
            zacetek = novo;
        } else {
            prejsnje->naslednje = novo;
        }
        prejsnje = novo;
        p++;
    }
    return zacetek;
}

void pocisti(Vozlisce* v) {
    if (v != NULL) {
        pocisti(v->naslednje);
        free(v);
    }
}

char* vNiz(Vozlisce* zacetek) {
    char* pNiz = NIZ;
    *pNiz++ = '[';
    Vozlisce* v = zacetek;
    while (v != NULL) {
        if (v != zacetek) {
            pNiz += sprintf(pNiz, ", ");
        }
        pNiz += sprintf(pNiz, "%d", v->podatek);
        v = v->naslednje;
    }
    sprintf(pNiz, "]");
    return NIZ;
}

void testiraj(Vozlisce* v, int element) {
    printf("%s", vNiz(v));
    v = pobrisiI(v, element);
    printf(" - %d -> %s\n", element, vNiz(v));
    pocisti(v);
}

int __main__() {
    testiraj(zgradi((int[]) {5, 5, 5, 7, 5, 7, 7, 7, 0}), 5);
    testiraj(zgradi((int[]) {5, 5, 5, 7, 5, 7, 7, 7, 0}), 7);
    testiraj(zgradi((int[]) {5, 5, 5, 7, 5, 7, 7, 7, 0}), 3);
    testiraj(zgradi((int[]) {42, 42, 42, 42, 42, 42, 0}), 42);
    testiraj(zgradi((int[]) {0}), 42);
    testiraj(zgradi((int[]) {1, 2, 3, 2, 1, 2, 3, 2, 0}), 1);
    testiraj(zgradi((int[]) {1, 2, 3, 2, 1, 2, 3, 2, 0}), 2);
    testiraj(zgradi((int[]) {1, 2, 3, 2, 1, 2, 3, 2, 0}), 3);
    testiraj(zgradi((int[]) {1, 1, 2, 3, 1, 1, 1, 4, 5, 0}), 1);
    testiraj(zgradi((int[]) {2, 3, 1, 1, 1, 4, 5, 1, 1, 0}), 1);

    exit(0);
    return 0;
}
