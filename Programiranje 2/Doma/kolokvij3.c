#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n;
    scanf("%d", &n);
    
    int a, s;
    scanf("%d %d", &a, &s);
    
    int** tabela = (int**)calloc(n, sizeof(int *));
    
    for (int i = 0; i < n; i++) {
        tabela[i] = (int*)calloc((n-i), sizeof(int));
        printf("%p - %d\n", tabela[i], i);
        tabela[i] -= i;
        printf("%p\n", tabela[i]);
        //*(tabela + i) = *(tabela + i) - i;
        
        for (int k = i; k < n; k++) {
            tabela[i][k] = k; 
            *(*(tabela + i) + k) = k;
        }
    }
    
    for (int i = 0; i < n; i++) {
        for (int k = 0; k < n; k++) {
            if (k >= i)
            printf("%d", tabela[i][k]);
            else
            printf(" ");
        }
        printf("\n");
    }
    
    printf("%d", tabela[a][s]);
    printf("\n");
    
    return 0;
}