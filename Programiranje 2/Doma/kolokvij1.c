#include <stdio.h>

int praSt(int x)
{
    int n = 2;
    while(n < x) {
        if(x % n == 0) {
            return 0;
        }
        n++;
    }
    return 1;
}

int main()
{
    int n = 1;
    int m = 0;
    int max = 0;
    while(scanf("%d", &n) && n != 0) {
        if (praSt(n)) {
            m++;
        }
        else {
            m = 0;
        }
        if (m > max) {
            max = m;
        }
    }
    printf("%d\n", max);
}