#include <stdio.h>
#include <stdlib.h>

int main()
{
    char* vzorcniNiz = (char*)malloc(101 * sizeof(char));
    scanf("%s", vzorcniNiz);
    
    int n, j, k, maks = 0;
    scanf("%d", &n);
    
    int rezultat = n;
    
    char** testniNizi = (char**)malloc(n * sizeof(char*));
    for (int i = 0; i < n; i++) {
        testniNizi[i] = (char*)malloc(101 * sizeof(char));
        scanf("%s", testniNizi[i]);
    }
    
    
    for (int i = 0; i < n; i++) {
        j = 0;
        k = 0;
        while (vzorcniNiz[j] != '\0' || testniNizi[i][k] != '\0') {
            if (vzorcniNiz[j] == '*') {
                maks = 0;
                while (vzorcniNiz[j] == '*') {
                    j++;
                }
                if (vzorcniNiz[j] == '\0') {
                    break;
                }
                while(testniNizi[i][k] != '\0') {
                    if (testniNizi[i][k] == vzorcniNiz[j] /*&& (testniNizi[i][k+1] == vzorcniNiz[j+1] || vzorcniNiz[j+1] == '\0')*/) {
                        maks = k;
                    }
                    k++;
                }
                j++;
                k = maks + 1;
            }
            else if (vzorcniNiz[j] == '?' && testniNizi[i][k] != '\0' || vzorcniNiz[j] == testniNizi[i][k]) {
                j++;
                k++;
            }
            else {
                printf("%s\n", testniNizi[i]);
                rezultat--;
                break;
            }
        }
    }
    printf("%d\n", rezultat);
    return 0;
}