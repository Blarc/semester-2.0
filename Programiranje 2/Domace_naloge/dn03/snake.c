#include <stdio.h>
#include <stdlib.h>

int main()
{
    int dolzinaKace = 1;
    int n;
    int stevec = 0;
    int stKorakov = 0;
    int moveX = 0;
    int moveY = 1;
    
    scanf("%d", &n);
    
    /* Tabela jabk */
    int **japka = (int **) malloc(n * sizeof(int *));
    for (int i = 0; i < n; i++) {
        japka[i] = (int *) malloc(3 * sizeof(int));
        scanf("%d", &japka[i][0]);  //x
        scanf("%d", &japka[i][1]);  //y
        scanf("%d", &japka[i][2]);  //type
    }
    
    scanf("%d", &stKorakov);
    
    /* Inicializacija kače */
    int **snake = (int **) malloc(sizeof(int *));
    snake[0] = (int *) malloc(2 * sizeof(int));
    
    snake[0][0] = 0;    //x
    snake[0][1] = 0;    //y
    
    while (stevec < stKorakov) {
        stevec++;
        int vrednost = 5;
        
        // preveri če se zaleti
        for (int i = 1; i < dolzinaKace; i++) {
            if ((snake[0][0] == snake[i][0]) && (snake[0][1] == snake[i][1])) {
                vrednost = 0;
                break;
            }
        }
        
        // preveri če poje japko
        for (int i = 0; i < n && vrednost; i++) {
            if ((snake[0][0] == japka[i][0]) && (snake[0][1] == japka[i][1])) {
                vrednost = japka[i][2];
                break;
            }
        }
        
        // kača se je zaletela
        if (vrednost == 0) {
            dolzinaKace = 0;
            break;
        }
        
        // povečaj tabelo in dolžino kače
        if (vrednost == 1) {
            dolzinaKace++;
            snake = (int **) realloc(snake, dolzinaKace * sizeof(int *));
            snake[dolzinaKace-1] = (int *) malloc(2 * sizeof(int));
            
        }
        
        if (vrednost == 2) {
            if (moveY == 1) {
                moveX = -1;
                moveY = 0;
            }
            else if (moveY == -1) {
                moveX = 1;
                moveY = 0;
            }
            else if (moveX == 1) {
                moveX = 0;
                moveY = 1;
            }
            else if (moveX == -1) {
                moveX = 0;
                moveY = -1;
            }
        }
        else if (vrednost == 3) {
            if (moveY == 1) {
                moveX = 1;
                moveY = 0;
            }
            else if (moveY == -1) {
                moveX = -1;
                moveY = 0;
            }
            else if (moveX == 1) {
                moveX = 0;
                moveY = -1;
            }
            else if (moveX == -1) {
                moveX = 0;
                moveY = 1;
            }
        }
        
        // premakne elemente v tabeli za eno v desno
        for (int i = (dolzinaKace-2); i > -1; i--) {
            snake[i+1][0] = snake[i][0];
            snake[i+1][1] = snake[i][1];
        }
        
        // prvi element nastavi na pozicijo kamor se premakne
        snake[0][0] += moveX;
        snake[0][1] += moveY;
    }
    printf("%d %d %d\n", dolzinaKace, snake[0][0], snake[0][1]);
    
    free(snake);
    free(japka);
}