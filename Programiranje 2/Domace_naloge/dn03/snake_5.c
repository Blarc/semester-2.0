#include <stdio.h>
#include <stdlib.h>

int absS(int x)
{
    return (x < 0) ? -x : x;
}

int main()
{   
    int stKorakov;
    int dolzinaKace = 1;
    int minJ = 1000000;
    int minS = 1000000;
    int smer = 0; //0 = gor, 1 = dol, 2 = levo, 3 = desno
    int inGame = 1;
    int eat;
    
    int n;
    scanf("%d", &n);
    
    
    int **japka = (int **) malloc(n * sizeof(int *));
    for (int i = 0; i < n; i++) {
        japka[i] = (int *) malloc(3 * sizeof(int));
        scanf("%d", &japka[i][0]);  //x
        scanf("%d", &japka[i][1]);  //y
        scanf("%d", &japka[i][2]);  //type
    }
    
    scanf("%d", &stKorakov);
    
    /*printf("Tabela jabk:\n");
    for (int i = 0; i < n; i++) {
        printf("%d %d %d\n", japka[i][0], japka[i][1], japka[i][2]);
    }*/
    //printf("//////////////\n");
    
    int **snake = (int **) malloc(sizeof(int *));
    snake[0] = (int *) malloc(2 * sizeof(int));
    
    snake[0][0] = 0;    //x
    snake[0][1] = 0;    //y
    
    //printf("Začetek: %d %d\n", snake[0][0], snake[0][1]);
    
    while (stKorakov > 0) {
        for (int j = 0; j < n; j++) {                                                                                   //check which fruit is the closest
            if (smer == 0 && japka[j][0] == snake[0][0] && snake[0][1] < japka[j][1] && japka[j][1] < minJ) {           //GOR
                minJ = japka[j][1];
                eat = j;
            }
            else if (smer == 1 && japka[j][0] == snake[0][0] && snake[0][1] > japka[j][1] && (-japka[j][1] < minJ)) {   //DOL
                minJ = japka[j][1];
                eat = j;
            }
            else if (smer == 2 && japka[j][1] == snake[0][1] && snake[0][0] > japka[j][0] && (-japka[j][0] < minJ)) {   //LEVO
                minJ = japka[j][0];
                eat = j;
            }
            else if (smer == 3 && japka[j][1] == snake[0][1] && snake[0][0] < japka[j][0] && japka[j][0] < minJ) {      //DESNO
                minJ = japka[j][0];
                eat = j;
            }
        }
        
        if (eat == 1000000) {
            //printf("%d\n", stKorakov);
            if (smer == 0) {
                snake[0][1] += stKorakov;
            }
            else if (smer == 1) {
                snake[0][1] -= stKorakov;
            }
            else if (smer == 2) {
                snake[0][0] -= stKorakov;
            }
            else {
                snake[0][0] += stKorakov;
            }
            break;
        }
        
        else if (japka[eat][2] == 2) {           //zavij levo
            if (smer == 0) {                //premika gor
                smer = 2;                   //zavije levo
            }
            else if (smer == 1) {           //premika dol
                smer = 3;                   //zavije desno
            }
            else if (smer == 2) {           //premika levo
                smer = 1;                   //zavije dol
            }
            else if (smer == 3) {           //premika desno
                smer = 0;                   //zavije gor
            }
            
        }
        else if (japka[eat][2] == 3) {      //zavij desno
            if (smer == 0) {                //premika gor
                smer = 3;                   //zavije desno
            }
            else if (smer == 1) {           //premika dol
                smer = 2;                   //zavije levo
            }
            else if (smer == 2) {           //premika levo
                smer = 0;                   //zavije gor
            }
            else if (smer == 3) {           //premika desno
                smer = 1;                   //zavije dol
            }
        }
        
        stKorakov -= (absS(snake[0][0] - japka[eat][0]) + absS(snake[0][1] - japka[eat][1]));
        
        snake[0][0] = japka[eat][0];
        snake[0][1] = japka[eat][1];
        printf("%d %d \n", snake[0][0], snake[0][1]);
        printf("Smer: %d\n", smer);
        
        minS = 1000000;
        minJ = 1000000;
        eat = 1000000;
    }
    printf("%d %d %d\n", dolzinaKace, snake[0][0], snake[0][1]);
}