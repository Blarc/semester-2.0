#include <stdio.h>
#include <stdlib.h>

int main()
{   
    int stKorakov;
    int dolzinaKace = 1;
    int minJ = 1000000;
    int minS = 1000000;
    int smer = 0; //0 = gor, 1 = dol, 2 = levo, 3 = desno
    int inGame = 1;
    int eat;
    
    int n;
    scanf("%d", &n);
    
    int **japka = (int **) malloc(n * sizeof(int *));
    for (int i = 0; i < n; i++) {
        japka[i] = (int *) malloc(3 * sizeof(int));
        scanf("%d", &japka[i][0]);  //x
        scanf("%d", &japka[i][1]);  //y
        scanf("%d", &japka[i][2]);  //type
    }
    
    scanf("%d", &stKorakov);
    
    int **snake = (int **) malloc(sizeof(int *));
    snake[0] = (int *) malloc(2 * sizeof(int));
    
    snake[0][0] = 0;    //x
    snake[0][1] = 0;    //y
    
    while (stKorakov > 0) {
        /*for (int j = 1; j < dolzinaKace; j++) {         //check if eats itself
            if (smer == 0) {        //GOR
                if (snake[0][0] == snake[j][0] && snake[0][0] < snake[j][0] && snake[j][0] < minS) {
                    minS = snake[j][0];
                }
            }
            else if (smer == 1) {   //DOL
                if (snake[0][0] == snake[j][0] && snake[0][0] > snake[j][0] && (-snake[j][0]) < minS) {
                    minS = snake[j][0];
                }
            }
            else if (smer == 2) {   //LEVO
                if (snake[0][1] == snake[j][1] && snake[0][1] < snake[j][1] && snake[j][1] < minS) {
                    minS = snake[j][1];
                }
            }
            else if (smer == 3) {   //DESNO
                if (snake[0][1] == snake[j][1] && snake[0][1] > snake[j][1] && (-snake[j][1]) < minS) {
                    minS = snake[j][1];
                }
            }
            else {
                printf("Kača se NE zaleti!");
            }
        }*/
        
        for (int j = 0; j < n; j++) {                   //check which fruit is the closest
            if (smer == 0) {        //GOR
                if (japka[j][0] == snake[0][0] && snake[0][0] < japka[j][0] && japka[j][0] < minJ) {
                    minJ = japka[j][0];
                    eat = j;
                }
            }
            else if (smer == 1) {   //DOL
                if (japka[j][0] == snake[0][0] && snake[0][0] > japka[j][0] && (-japka[j][0]) < minJ) {
                    minJ = japka[j][0];
                    eat = j;
                }
            }
            else if (smer == 2) {   //LEVO
                if (japka[j][1] == snake[0][1] && snake[0][1] < japka[j][1] && japka[j][1] < minJ) {
                    minJ = japka[j][1];
                    eat = j;
                }
            }
            else if (smer == 3) {   //DESNO
                if (japka[j][1] == snake[0][1] && snake[0][1] > japka[j][1] && (-japka[j][1]) < minJ) {
                    minJ = japka[j][1];
                    eat = j;
                }
            }
            else {
                //prištej
                printf("V tej smeri ni jabolka!");
                break;
            }
        }
        
        /*if (minS < minJ) {          //if eats itself instead of the fruit, stop
            break;
            printf("Kača se je zaletela sama vase!");
        }*/
        
        snake[0][0] = japka[eat][0];
        snake[0][1] = japka[eat][1];
        
        /*if (japka[eat][2] == 1) {           //sadež
            dolzinaKace++;
            
        }*/
        if (japka[eat][2] == 2) {      //zavij levo
            if (smer == 0) {                //premika gor
                smer = 2;                   //zavije levo
            }
            else if (smer == 1) {           //premika dol
                smer = 3;                   //zavije desno
            }
            else if (smer == 2) {           //premika levo
                smer = 1;                   //zavije dol
            }
            else if (smer == 3) {           //premika desno
                smer = 0;                   //zavije gor
            }
            
        }
        else if (japka[eat][2] == 3) {      //zavij desno
            if (smer == 0) {                //premika gor
                smer = 3;                   //zavije desno
            }
            else if (smer == 1) {           //premika dol
                smer = 2;                   //zavije levo
            }
            else if (smer == 2) {           //premika levo
                smer = 0;                   //zavije gor
            }
            else if (smer == 3) {           //premika desno
                smer = 1;                   //zavije dol
            }
        }
        else {
            printf("Napaka pri typu japka!");
            break;
        }
        minS = 1000000;
        minJ = 1000000;
    }
    
    printf("%d %d\n", snake[0][0], snake[0][1]);
    free(snake);
    free(japka);
}