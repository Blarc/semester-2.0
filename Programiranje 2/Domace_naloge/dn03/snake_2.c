#include <stdio.h>
#include <stdlib.h>

int main()
{
    int dolzinaKace = 1;
    int min = 1000000;
    int stranica = 10000;
    int n;
    int stevec = 0;
    int stKorakov = 0;
    int moveX = 0;
    int moveY = 1;
    
    scanf("%d", &n);
    
    /* Inicializacija mreže */
    int **mreza = (int **) calloc(stranica, stranica * sizeof(int *));
    for (int i = 0; i < stranica; i++) {
        mreza[i] = (int *) calloc(stranica, stranica * sizeof(int));
    }
    
    /* Vnos japk */
    for (int i = 0; i < n; i++) {
        int x, y, t;
        scanf("%d %d %d", &x, &y, &t);
        mreza[stranica/2 + x][stranica/2 + y] = t;
    }
    
    scanf("%d", &stKorakov);
    
    /* Inicializacija kače */
    int **snake = (int **) malloc(sizeof(int *));
    snake[0] = (int *) malloc(2 * sizeof(int));
    
    snake[0][0] = stranica/2;    //x
    snake[0][1] = stranica/2;    //y
    
    /*for (int i = 0; i < stranica; i++) {
        for (int j = 0; j < stranica; j++) {
            if (mreza[i][j] > 0) 
                printf("[%d,%d]: %d\n", i-stranica/2, j-stranica/2, mreza[i][j]);
        }
    }*/
    
    //printf("%d %d\n", snake[0][0] - stranica/2, snake[0][1] - stranica/2);
    while (stevec < stKorakov) {
        int vrednost = mreza[snake[0][0] + moveX][snake[0][1] + moveY];
        //printf("Vrednost: %d\n", vrednost);
        if (vrednost == 2) {
            n--;
            snake[0][0] = snake[0][0] + moveX;
            snake[0][1] = snake[0][1] + moveY;
            //printf("%d %d\n", snake[0][0] - stranica/2, snake[0][1] - stranica/2);
            //printf("Zavij levo\n");
            
            if (moveY == 1) {
                moveX = -1;
                moveY = 0;
            }
            else if (moveY == -1) {
                moveX = 1;
                moveY = 0;
            }
            else if (moveX == 1) {
                moveX = 0;
                moveY = 1;
            }
            else if (moveX == -1) {
                moveX = 0;
                moveY = -1;
            }
            //printf("moveX: %d\nmoveY: %d\n", moveX, moveY);
        }
        else if (vrednost == 3) {
            n--;
            snake[0][0] = snake[0][0] + moveX;
            snake[0][1] = snake[0][1] + moveY;
            //printf("%d %d\n", snake[0][0] - stranica/2, snake[0][1] - stranica/2);
            //printf("Zavij desno\n");
            
            if (moveY == 1) {
                moveX = 1;
                moveY = 0;
            }
            else if (moveY == -1) {
                moveX = -1;
                moveY = 0;
            }
            else if (moveX == 1) {
                moveX = 0;
                moveY = -1;
            }
            else if (moveX == -1) {
                moveX = 0;
                moveY = 1;
            }
            //printf("moveX: %d\nmoveY: %d\n", moveX, moveY);
        }
        else {
            snake[0][0] = snake[0][0] + moveX;
            snake[0][1] = snake[0][1] + moveY;
            //printf("%d %d\n", snake[0][0] - stranica/2, snake[0][1] - stranica/2);
        }
        stevec++;
    }
    printf("%d %d %d\n", dolzinaKace, snake[0][0] - stranica/2, snake[0][1] - stranica/2);
    free(mreza);
    free(snake);
}