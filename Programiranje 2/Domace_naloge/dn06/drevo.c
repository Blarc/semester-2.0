#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int ime;
    int st_otrok;
    int* childs;
} node;

int sendNodes(node atm, node* allNodes)
{
    if (atm.st_otrok == 0) {
        return 1;
    }
    int cifra = 1;
    for (int i = 0; i < atm.st_otrok; i++) {
        cifra += sendNodes(allNodes[atm.childs[i]], allNodes);
    }
    return cifra;
}

int sendNodeSize(node atm, node* allNodes)
{
    if (atm.st_otrok == 0) {
        return 0;
    }
    int maks = 1;
    for (int i = 0; i < atm.st_otrok; i++) {
        int trenuten = sendNodeSize(allNodes[atm.childs[i]], allNodes) + 1;
        if (maks < trenuten) {
            maks = trenuten;
        }
    }
    return maks;
}

int sendBigNode(node atm, node* allNodes)
{
    if (atm.st_otrok == 0) {
        return atm.ime;
    }
    int maks = atm.ime;
    for (int i = 0; i < atm.st_otrok; i++) {
        int trenuten = sendBigNode(allNodes[atm.childs[i]], allNodes);
        if (trenuten > maks) {
            maks = trenuten;
        }
    }
    return maks;
}

int sendNippleSize(node atm, node* allNodes)
{
    /*int nivo = sendNodeSize(allNodes[atm], allNodes);
    if (atm.st_otrok == 0) {
        return ;
    }
    for (int i = 0; i < atm.st_otrok; i++) {
        sendNippleSize(allNodes[atm.childs[i]], allNodes);
    }*/
    return 1;
}


int main()
{   
    node* allNodes = malloc(1000 * sizeof(node));
    int n;
    scanf("%d", &n);
    
    for (int i = 1; i <= n; i++) {
        allNodes[i].ime = i;
        int st_otrok;
        scanf("%d", &st_otrok);

        int* childs = malloc(st_otrok * sizeof(int));
        int ime_otroka;
        for (int j = 0; j < st_otrok; j++) {
            scanf("%d", &ime_otroka);
            childs[j] = ime_otroka;
        }
        allNodes[i].st_otrok = st_otrok;
        allNodes[i].childs = childs;

    }
    
    int k;
    scanf("%d", &k);
    
    for (int i = 0; i < k; i++) {
        int ukaz, arg, res;
        scanf("%d %d", &ukaz, &arg);
        switch(ukaz)
        {
            case 1:
                //izpise stevilo vozlisc v podanem poddrevesu
                res = sendNodes(allNodes[arg], allNodes);
                printf("%d\n", res);
                break;
            case 2:
                //izpise vozlisce z najvecjo stevilko v podanem poddrevesu
                res = sendBigNode(allNodes[arg], allNodes);
                printf("%d\n", res);
                break;
            case 3:
                //izpise visino podanega poddrevesa
                res = sendNodeSize(allNodes[arg], allNodes);
                printf("%d\n", res);
                break;
            case 4:
                //izpise stevilo vozlisc na nivoju h v podanem poddrevesu, kjer je h višina poddrevesa
                res = sendNippleSize(allNodes[arg], allNodes);
                printf("%d\n", res);
                break;
            case 5:
                //izpise poddrevo
                break;
        }
    }
    
    /*for (int i = 1; i <= n; i++) {
        printf("Node %d ima %d otroke ", i, allNodes[i].st_otrok);
        printf("[ ");
        for (int j = 0; j < allNodes[i].st_otrok; j++) {
            printf("%d ", allNodes[i].childs[j]);
        }
        printf("]\n");
    }*/
    for (int i = 0; i < n; i++) {
        free(allNodes[i].childs);
    }
    free(allNodes);
}