#include <stdio.h>

int expN(long n) {
    int m = 0;
    while (n > 9) {
        m++;
        n /= 10;
    }
    return m;
}

long powN(int n) {
    long m = 1;
    for (int i = 0; i < n; i++) {
        m *= 10;
    }
    return m;
}

int main()
{
    long x, y;
    scanf("%ld %ld", &x, &y);
    int expY = expN(y);
    int expX = expN(x);
    long m, n;

    for (int i = expY; i >= 0; i--) {
        if (i > 0) {
            m = y / powN(i);
            n = x / powN(expX - m + 1);
            printf("%ld\n", n);
            
            x -= n * powN(expX - m + 1);
            y -= m * powN(i);
            expX -= m;
        } else {
            printf("%ld\n", x);
        }
    }
    return 0;
}
