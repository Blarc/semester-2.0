#include <stdio.h>

long long zrcali(long long n)
{
    long long z = 0;
    while (n > 0) {
        z = z * 10 + n % 10;
        n /= 10;
    }
    return z;
}



int main()
{
    long long x, y;
    int c = 0;
    scanf("%lld %lld", &x, &y);
    x = zrcali(x);
    y = zrcali(y);
    //printf("%lld\n", x);
    
    while(y > 0) {
        c = y % 10;
        for(int i = 0; i < c; i++) {
            if (x % 10 != 0 || c == 1)
                printf("%lld", x % 10);
            else if (x % 10 == 0 && i != 0)
                printf("%lld", x % 10);
            x /= 10;
        }
        printf("\n");
        y /= 10;
    }
}