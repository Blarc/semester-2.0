#include <stdio.h>

int kodiraj()
{
    char c;
    int n = 1;    
    char ex = getchar();

    while((c = getchar()) != EOF) {
        if (c == ex) {
            n++;
        } else {
            if (ex == '#') printf("##%d#", n);
            else if (n > 4) {
                printf("#%c%d#", ex, n);
            } else for (int i = 0; i < n; i++) putchar(ex);
            n = 1;
        }
        ex = c;
    }
    return 0;
}

int dekodiraj()
{
    int num;
    char c;
    while((c = getchar()) != EOF) {
       if (c == '#') {
            char x = getchar();
            scanf("%d", &num);
            for (int i = 0; i < num; i++) {
                putchar(x);
            }
            getchar();
       } else {
            putchar(c);
       }
    }
    return 0;
}

int main()
{   
    char u = getchar();
    getchar();

    switch(u) {
        case '1':
            kodiraj();
        case '2':
            dekodiraj();
    }

    return 0;
}