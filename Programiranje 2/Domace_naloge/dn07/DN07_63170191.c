#include <stdio.h>
#include <stdlib.h>

typedef struct datum
{
	int dan;
	int mesec;
	int leto;
	char* izpis;

} datum;

typedef struct clan
{
    int st_knjig;
    datum* datumi;

} clan;

typedef struct knjiga
{
	int st_na_voljo;
	clan* clani;

} knjiga;

datum narediDatum(char* novo, int d)
{
	datum novDatum;
	novDatum.dan = 10 * (novo[0] - '0') + (novo[1] - '0');
	novDatum.mesec = 10 * (novo[3] - '0') + (novo[4] - '0');
	novDatum.leto = 1000 * (novo[5] - '0') + 100 * (novo[6] - '0') + 10 * (novo[7] - '0') + (novo[8] - '0');

	novDatum.izpis = malloc(11 * sizeof(char));
	novDatum.izpis = novo;
	return novDatum;
}

char izposojaKnjige(clan* clani, knjiga* knjige, int d)
{
	char* datum = (char*)malloc(11 * sizeof(char));
	int indeks_clana;
	int indeks_knjige;
	scanf("%s %d %d\n", datum, &indeks_clana, &indeks_knjige);

	//preveri ce je knjiga na voljo
	if (knjige[indeks_knjige].st_na_voljo == 0) {
		return 'N';
	}

	//preveri ce clan ze ima izbrano knjigo
	if (clani[indeks_clana].datumi[indeks_knjige].dan != 0) {
		return 'N';
	}

	//update everything
	knjige[indeks_knjige].st_na_voljo--;
	clani[indeks_clana].datumi[indeks_knjige] = narediDatum(datum, d);
	clani[indeks_clana].st_knjig += 1;

	return 'D';
}

void vrnitevKnjige(clan* clani, knjiga* knjige, int d)
{
	char* datum = (char*)malloc(11 * sizeof(char));
	int indeks_clana;
	int indeks_knjige;
	scanf("%s %d %d\n", datum, &indeks_clana, &indeks_knjige);

	//preveri ce clan sploh ima knjigo
	if (clani[indeks_clana].datumi[indeks_knjige].dan == 0) {
		printf("N\n");
	}

	//update everything
	else {
		knjige[indeks_knjige].st_na_voljo++;
		clani[indeks_clana].datumi[indeks_knjige] = narediDatum("00.00.0000", d);
		clani[indeks_clana].st_knjig -= 1;
		printf("0\n");
	}
}

int main()
{
	int c; //st clanov
	int k; //st knjiznih naslovov
	int p; //st izvodov
	int d; //najvec dni izposoje
	int z; //zamudnina
	int v; //st vrstic

	scanf("%d %d %d %d %d %d\n", &c, &k, &p, &d, &z, &v);

	//izposojenih izvodov 
	clan* clani = (clan*)malloc((c + 1) * sizeof(clan));
	for (int i = 1; i <= c; i++) {
		clani[i].st_knjig = 0;
		clani[i].datumi = calloc(k, k * sizeof(datum));
	}

	knjiga* knjige = (knjiga*)malloc((k + 1) * sizeof(knjiga));
	for (int i = 1; i <= k; i++) {
		knjige[i].st_na_voljo = p;
		knjige[i].clani = malloc(p * sizeof(clan));
	}

	for (int i = 0; i < v; i++) {
		char ukaz = getchar();
		int atm;
		if (ukaz == '+') {
			printf("%c\n", izposojaKnjige(clani, knjige, d));
		}
		else if (ukaz == '-') {
			vrnitevKnjige(clani, knjige, d);
		}
		else if (ukaz == 'A') {
			scanf("%d\n", &atm);
			printf("%d\n", clani[atm].st_knjig);
		}
		else if (ukaz == 'B') {
			scanf("%d\n", &atm);
			printf("%d\n", p - knjige[atm].st_na_voljo);	
		}
		else if (ukaz == 'C') {
			scanf("%d\n", &atm);
			printf("/");
			for (int i = 0; i < k && clani[atm].st_knjig > 0; i++) {
				if (clani[atm].datumi[i].dan > 0) {
					printf("%d:%s/", i, clani[atm].datumi[i].izpis);
				}
			}
			printf("\n");
		}
	}
	return 0;
}