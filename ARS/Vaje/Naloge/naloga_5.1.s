.data
.org 0x40001000
TAB: .byte 17,9,20,7,1
VEC: .byte 0
BIT: .byte 0

.code
.org 0x0

lhi r1, TAB
addui r1, r1, TAB

lhi r2, VEC
addui r2, r2, VEC
lb r3, 0(r2)

lhi r6, BIT
addui r6, r6, BIT
lb r7, 0(r6)

lb r4, 0(r1)
sgti r5, r4, 10
addu r3, r3, r5

andi r5, r4, 20
addu r7, r7, r5

lb r4, 1(r1)
sgti r5, r4, 10
addu r3, r3, r5

andi r5, r4, 20
addu r7, r7, r5

lb r4, 2(r1)
sgti r5, r4, 10
addu r3, r3, r5

andi r5, r4, 20
addu r7, r7, r5

lb r4, 3(r1)
sgti r5, r4, 10
addu r3, r3, r5

andi r5, r4, 20
addu r7, r7, r5

lb r4, 4(r1)
sgti r5, r4, 10
addu r3, r3, r5

andi r5, r4, 20
addu r7, r7, r5



halt