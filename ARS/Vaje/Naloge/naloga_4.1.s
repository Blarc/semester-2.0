.data
.org 0x400
ST1: .word16 0x913
ST2: .word16 0x10F7
ST3: .byte 0x81
ST4: .byte 0x6
.align 4
ST5: .word 0x5BA0
ST6: .word 0xA8CA
SUM16: .space 4
MUL: .space 4
DIV: .space 4
SUM: .space 4
DIF: .space 4

.code
.org 0x0
lh R1, ST1(R0)
lh R2, ST2(R0)
lb R3, ST3(R0)
lb R4, ST4(R0)
lw R5, ST5(R0)
lw R6, ST6(R0)

addu R7, R1, R2
sh SUM16(R0), R7

sll R7, R3, R4
sh MUL(R0), R7

srai R7, R3, 5
sh DIV(R0), R7

halt