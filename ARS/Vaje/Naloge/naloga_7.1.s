;PUSH Rx:
	;sw 0(r30), Rx
	;subui r30, r30, #4

;POP Rx:
	;addui r30, r30, #4
	;lw Rx, 0(r30)

	.data
	.org 0x400
N:	.word 4 ;r24
REZ: .space 4
		
	.code
	.org 0
	addui r30, r0, #0x4FC; inicializacija skladovnega kazalca

	lw r24, N(r0)
	call r31, FAK(r0)
	addui r30, r30, #4
	sw REZ(r0), r28
	halt



FAK:;push r31
	sw 0(r30), r31
	subui r30, r30, #4
	;push r29; kazalec na okvir, framepointer
	sw 0(r30), r29
	subui r30, r30, #4
	addu r29, r0, r30
	;push r6; te štiri vrstice napišemo vedno
	sw 0(r30), r6
	subui r30, r30, #4
	;push r7
	sw 0(r30), r7
	subui r30, r30, #4

	bne r24, ELSE

IF:	addui r28, r0, #1
	j KONEC(r0)

ELSE: add r6, r0, r24
	  call r31, FAK(r0)

FOR:  add r7, r7, r28
	  subi r6, r6, #1
	  beq r6, KONEC
	  j FOR(r0)

KONEC: 
	;pop r7
	addui r30, r30, #4
	lw r7, 0(r30)
	;pop r6
	addui r30, r30, #4
	lw r6, 0(r30)
	;pop r29
	addui r30, r30, #4
	lw r29, 0(r30)
	;pop r31
	addui r30, r30, #4
	lw r31, 0(r30)
	j 0(r31)
