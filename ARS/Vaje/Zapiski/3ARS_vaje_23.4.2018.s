;PUSH Rx:
	;sw 0(r30), Rx
	;subui r30, r30, #4

;POP Rx:
	;addui r30, r30, #4
	;lw Rx, 0(r30)

; r24 parameter 1
; r25 parameter 2
; r31 povratni naslov
; r29 kazalec na okvir
; r28 vrnjena vrednost podprograma
; (r26, r27) nebomo uporabljali

;int sestej (int a, int b, int c) {
;	return a + b + c;
;}

	.data
	.org 0x400
a:	.word 5
b:	.word 6
c:	.word 7
rez: .space 4

	.code
	.org 0
	addui r30, r0, #0x4FC; inicializacija skladovnega kazalca
	lw r24, a(r0)
	lw r25, b(r0)
	lw r1, c(r0)
	;push r1; swo(r30), r1, subui r30, r30, 4
	sw 0(r30), r1
	subui r30, r30, #4
	call r31, sestej(r0)
	addui r30, r30, 4
	sw rez(r0), r28
	halt

SESTEJ: ;push r31
		sw 0(r30), r31
		subui r30, r30, #4
		;push r29; kazalec na okvir, framepointer
		sw 0(r30), r29
		subui r30, r30, #4
		addu r29, r0, r30
		;push r6; te štiri vrstice napišemo vedno
		sw 0(r30), r6
		subui r30, r30, #4

		lw r6, 12(r29); r6 <- M[C]
		add r6, r6, r24; a + c
		add r28, r6, r25; a + c + b; jedro podprograma

		;pop r6
		addui r30, r30, #4
		lw r6, 0(r30)
		;pop r29
		addui r30, r30, #4
		lw r29, 0(r30)
		;pop r31
		addui r30, r30, #4
		lw r31, 0(r30)
		j 0(r31)
